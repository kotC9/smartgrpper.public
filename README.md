**System Requirements**
*  .NET Framework 4.6.1
*  Microsoft Visual C++ 2017 Redistributable x64

**GPU requirements (optional)**
* Nvidia CUDA Toolkit 10.0
* Nvidia cuDNN v7.4.2.24 for CUDA 10.0

**Build requirements**
*  Visual studio (build in VS2019)

**Packages used**
*  [Alturos.Yolo](https://github.com/AlturosDestinations/Alturos.Yolo) (Yolov3 wrapper to c#)
*  [VrepWrapper](https://github.com/wbadry/V-REP-Remote-API-for-Dot-NET) (with some fixes)
*  Mysql (then change to LiteDB)
*  [Yandex.SpeechKit](https://cloud.yandex.ru/services/speechkit) (api key deleted in code)

**Directory Structure**
```
  ├── SmartGripper                #main project
  |     ├──Common                 #common files (remove pls)
  |     ├──Robot                  #robot control (must be moved to a new project)
  |     ├──UserForm               #UI
  ├── SmartGripper.VrepWrapper    #forked VrepWrapper with some fixes
```

**Contacts**
*  Email svssys@ya.ru