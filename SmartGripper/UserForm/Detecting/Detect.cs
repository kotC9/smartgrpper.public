﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using Alturos.Yolo;
using Alturos.Yolo.Model;
using Emgu.CV;

/* kotC9 */

namespace SmartGripper.UserForm.Detecting
{
    /// <summary>
    /// General class for detecting items
    /// </summary>
    public class Detect
    {
        private YoloWrapper _yoloWrapper;

        public Detect()
        {
            //before detect, init Yolo
            InitYoloAsync();
        }

        public async void InitYoloAsync()
        {
            await Task.Run( () =>
            {
                //TODO: add training system for custom objects
                _yoloWrapper = new YoloWrapper("yolov3.cfg", "yolov3.weights", "coco.names", ignoreGpu: false);
            });
        }

        /// <summary>
        /// Detects items
        /// </summary>
        /// <returns>Founded items</returns>
        public IEnumerable<YoloItem> DetectObjects(byte[] image)
        {
            var items = _yoloWrapper.Detect(image);
            
            return items;
        }

        /// <summary>
        /// Convert bitmap to byte array
        /// </summary>
        /// <returns> byte array</returns>
        public byte[] ConvertBitmapToBytes(Bitmap bitmap)
        {
            var ms = new MemoryStream();
            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }

        /// <summary>
        /// Take a photo from the camera
        /// </summary>
        /// <returns>Bitmap</returns>
        public Bitmap MakeBitmap()
        {
            var capture = new VideoCapture();
            var bitmap = capture.QueryFrame().Bitmap;
            capture.Dispose();
            return bitmap;
        }

        /// <summary>
        /// Take a photo from the camera and save it
        /// </summary>
        /// <returns>
        /// *.jpg, *.png, *.bmp or another type image
        /// </returns>
        public void TakePhoto(string name)
        {
            var capture = new VideoCapture();
            capture.QueryFrame().Save(name);
            capture.Dispose();
        }
    }
}
