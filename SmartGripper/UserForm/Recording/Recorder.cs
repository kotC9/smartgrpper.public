﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NAudio.Wave;

namespace SmartGripper.UserForm.Recording
{
    class Recorder
    {
        private WaveIn recorder;
        private BufferedWaveProvider bufferedWaveProvider;
        private SavingWaveProvider savingWaveProvider;
        private WaveOut player;

        /// <summary>
        /// convert .ogg file to byte array
        /// </summary>
        /// <returns>byte[]</returns>
        public byte[] OggToByteArray(string path)
        {
            return File.ReadAllBytes(path);
        }

        /// <summary>
        /// Start speech recording, example on click button
        /// </summary>
        public void OnStartRecording()
        {
            recorder = new WaveIn();
            recorder.DataAvailable += RecorderOnDataAvailable;

            // set up our signal chain
            bufferedWaveProvider = new BufferedWaveProvider(recorder.WaveFormat);
            savingWaveProvider = new SavingWaveProvider(bufferedWaveProvider, @"D:\Users\kotc9-lapka\Documents\speech1.ogg");

            // set up playback
            player = new WaveOut();
            player.Init(savingWaveProvider);

            // begin playback & record
            player.Play();
            recorder.StartRecording();
        }

        /// <summary>
        /// Stop speech recording, example on click button
        /// </summary>
        public void OnStopRecording()
        {
            recorder?.StopRecording();
            // stop playback
            player?.Stop();
            // finalise the WAV file
            savingWaveProvider?.Dispose();
        }

        private void RecorderOnDataAvailable(object sender, WaveInEventArgs waveInEventArgs)
        {
            bufferedWaveProvider.AddSamples(waveInEventArgs.Buffer, 0, waveInEventArgs.BytesRecorded);
        }
    }
}
