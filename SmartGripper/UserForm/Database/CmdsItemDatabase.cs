﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartGripper.Common.Models;
using SmartGripper.UserForm.Models;

namespace SmartGripper.UserForm.Database
{
    public class CmdsItemDatabase
    {
        /// <summary>
        /// add item to db
        /// </summary>
        /// <param name="name">item name</param>
        /// <param name="gripperType">type - 0 or 1, see SmartGripper.Common.Models.GripperType</param>
        public static bool AddItem(string itemClass, int gripperType)
        {
            using (var db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                db.DbItems.Add(new ItemDatabase {ItemClass = itemClass, GripperType = gripperType});
                db.SaveChanges();
                return true;
            }
        }

        public static bool FindItem(string itemClass, ref int gripperType)
        {
            using (var db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                var items = db.DbItems.ToList();

                //check users
                foreach (var item in items)
                {
                    if (itemClass != item.ItemClass) continue;

                    gripperType = item.GripperType;
                    return true;
                }
            }

            //if can't connect to db or not found item
            return false;
        }

        /// <summary>
        /// get item names
        /// </summary>
        public static bool GetItemList(ref List<string> itemsList)
        {
            using (var db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                var items = db.DbItems.ToList();
                foreach (var item in items)
                    itemsList.Add(item.ItemClass);
                return true;
            }
        }

        //TODO: remove this method in release
        public static bool AddTestItems()
        {
            using (var db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                
                
                var item1 = new ItemDatabase { ItemClass = "bottle", GripperType = 0 };
                var item2 = new ItemDatabase { ItemClass = "123", GripperType = 0};
                //var item3 = new ItemDatabase { ItemClass = "1234", GripperType = (int)GripperType.SuckerGripper };

                db.DbItems.Add(item1);
                db.DbItems.Add(item2);
                //db.DbItems.Add(item3);
                db.SaveChanges();
                Console.WriteLine("Объекты успешно сохранены");
                return true;
            }
        }
    }
}
