﻿using Microsoft.EntityFrameworkCore;
using SmartGripper.UserForm.Models;

namespace SmartGripper.UserForm.Database
{
    public class ApplicationContext : DbContext
    {
        //list items
        public DbSet<ItemDatabase> DbItems { get; set; }

        //list commands
        public DbSet<CommandDatabase> DbCommands { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySQL("server=localhost;UserId=root;Password=;database=smartgripper;");
        }
    }
}
