﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartGripper.UserForm.Models;

namespace SmartGripper.UserForm.Database
{
    class SpeechCmdsDatabase
    {

        /// <summary>
        /// add command to db
        /// </summary>
        public static bool AddCommand(string command, int taskType)
        {
            using (var db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                db.DbCommands.Add(new CommandDatabase());
                db.SaveChanges();
                return true;
            }
        }

        public static bool FindCommand(string command, ref CommandDatabase commandTask)
        {
            using (var db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                var items = db.DbCommands.ToList();

                //check users
                foreach (var item in items)
                {
                    if (command != item.Name) continue;

                    commandTask = item;
                    return true;
                }
            }

            //if can't connect to db or not found item
            return false;
        }

        /// <summary>
        /// get item names
        /// </summary>
        public static bool GetCommandList(ref List<string> commandList)
        {
            using (var db = new ApplicationContext())
            {
                db.Database.EnsureCreated();
                var items = db.DbCommands.ToList();
                foreach (var item in items)
                    commandList.Add(item.Name);
                return true;
            }
        }


        //TODO: remove this method in release
        public static bool AddTestCommands()
        {
            using (var db = new ApplicationContext())
            {
                db.Database.EnsureCreated();


                var command1 = new CommandDatabase {Name = "взять все яблоки", Task = 0 };
                var command2 = new CommandDatabase {Name = "сортировать", Task = 0 };
                var command3 = new CommandDatabase {Name = "взять все купюры", Task = 0 };

                db.DbCommands.Add(command1);
                db.DbCommands.Add(command2);
                db.DbCommands.Add(command3);
                db.SaveChanges();
                Console.WriteLine("Объекты успешно сохранены");
                return true;
            }
        }
    }
}
