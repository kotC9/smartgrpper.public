﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using Emgu.CV;
using Emgu.CV.CvEnum;
using SmartGripper.VrepWrapper;
using Emgu.CV.Structure;
using NAudio.Wave.Asio;
using SmartGripper.UserForm.Models;

namespace SmartGripper.UserForm.VREP
{
    /// <summary>
    /// This class describes the actions for V-REP
    /// </summary>
    public class CmdsVrep
    {
        private int _clientId;
        private int _visionSensor;
        public bool ConnectToVrep()
        {
            KVrep.simxFinish(-1);
            _clientId = KVrep.SimStart("127.0.0.1", 19997, true, true, 5000, 5);
            _visionSensor = KVrep.SimGetObjectHandle(_clientId, "v0");
            Console.WriteLine(_visionSensor);

            //without this line, image receiving does not work
            KVrep.SimGetVisionSensorImage(_clientId, _visionSensor, out _, out _, '\0');

            if (_clientId == -1) return false;
            KVrep.SimAddStatusbarMessage(_clientId, "CONNECTED");
            return true;
        }

        public void FinishVrep()
        {
            //TODO: simstopsimulation
            KVrep.simxFinish(_clientId);
        }

        public byte[] GetImageFromCamera()
        {
            if (_clientId == -1) return new byte[]{};
            KVrep.SimGetVisionSensorImage(_clientId, _visionSensor, out var b, out var returnImage, '\0');


            var image = new Image<Rgb, byte>(b,b,3*b,returnImage);
            
            var temp = image.Flip(FlipType.Vertical);
            temp.Save("123.jpg");
            return temp.Bytes;

            //var joint1 = KVrep.SimGetObjectHandle(_clientId, "UR10_link4_visible");

            //KVrep.SimSetJointTargetVelocity(_clientId, joint1, 100f);
            //KVrep.SimSetJointTargetPosition(_clientId, joint1, 0.3f);
        }

        /// <summary>
        /// функция для перемещения захвата в определенную точку
        /// </summary>
        /// <param name="toPosition">position where you want to move the gripper</param>
        public void MoveToPos(Point3D toPosition)
        {
            //Костыль: in remote api no function rmlMoveToJointPositions()
            //send position as string, in V-REP get value 

            var a = KVrep.SimSetStringSignal(_clientId, "manipulatorPos", $"{toPosition.X} {toPosition.Y} {toPosition.Z} 0");
            Console.WriteLine(a);
        }

        public bool OkTask()
        {
            var result = KVrep.SimGetIntegerSignal(_clientId, "MovedRobotOk");
            return result != 0;
        }

        /// <summary>
        /// функция для захвата
        /// </summary>
        public void GrabItem()
        {
            //TODO: magic algorithm
        }

        /// <summary>
        /// функция для захвата
        /// </summary>
        public void DropItem()
        {
            //TODO: magic algorithm
        }
    }
}
