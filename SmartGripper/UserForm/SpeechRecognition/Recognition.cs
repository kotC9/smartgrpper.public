﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using NAudio.Wave;
using SmartGripper.Common.GlobalVariables;
using SmartGripper.Common.Models;
using SmartGripper.UserForm.Database;
using SmartGripper.UserForm.Models;

/* kotC9 */

namespace SmartGripper.UserForm.SpeechRecognition
{
    public class Recognition
    {
        /// <summary>
        /// Send byte array audio to yandex.speechkit
        /// </summary>
        /// <returns>text from audio</returns>
        public static string YandexApiRecognition(byte[] audio)
        {
            using (var client = new HttpClient())
            {
                //enter your api key for yandex.speechkit
                var apiKey = "";
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                    apiKey);
                client.DefaultRequestHeaders.TransferEncodingChunked = true;

                var content = new ByteArrayContent(audio);
                //TODO: do it async.
                var response = client
                    .PostAsync(
                        "https://stt.api.cloud.yandex.net/speech/v1/stt:recognize?topic=general&folderId=b1g4evj46j4c9956t0n8&lang=ru-RU",
                        content).Result;
                return response.Content.ReadAsStringAsync().Result;
            }
        }

        /// <summary>
        /// checks founded text for existing commands
        /// </summary>
        /// <param name="text">text to check</param>
        public static bool CheckTextSpeech(string text)
        {
            var _ = new CommandDatabase();
            if (!SpeechCmdsDatabase.FindCommand(text, ref _)) return false;

            //foreach(var task in _.Task)
            GlobalVariables.ItemsToDetecting.Add(GlobalVariables.ListItems[_.Task]);

            return true;
        }


    }
}
