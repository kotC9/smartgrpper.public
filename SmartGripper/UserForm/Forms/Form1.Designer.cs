﻿namespace SmartGripper.UserForm.Forms
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.ButtonStart = new System.Windows.Forms.Button();
            this.StopRec = new System.Windows.Forms.Button();
            this.StartRec = new System.Windows.Forms.Button();
            this.debugX = new System.Windows.Forms.TextBox();
            this.debugY = new System.Windows.Forms.TextBox();
            this.debugZ = new System.Windows.Forms.TextBox();
            this.statusCode = new System.Windows.Forms.Label();
            this.connectButton = new System.Windows.Forms.Button();
            this.updateData = new System.Windows.Forms.Button();
            this.finishVrep = new System.Windows.Forms.Button();
            this.imageBox1 = new Emgu.CV.UI.ImageBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(184, 196);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "There recognition text";
            // 
            // ButtonStart
            // 
            this.ButtonStart.Location = new System.Drawing.Point(12, 394);
            this.ButtonStart.Name = "ButtonStart";
            this.ButtonStart.Size = new System.Drawing.Size(140, 44);
            this.ButtonStart.TabIndex = 1;
            this.ButtonStart.Text = "Start system";
            this.ButtonStart.UseVisualStyleBackColor = true;
            this.ButtonStart.Click += new System.EventHandler(this.StartButton);
            // 
            // StopRec
            // 
            this.StopRec.Location = new System.Drawing.Point(12, 256);
            this.StopRec.Name = "StopRec";
            this.StopRec.Size = new System.Drawing.Size(136, 43);
            this.StopRec.TabIndex = 3;
            this.StopRec.Text = "Stop recording";
            this.StopRec.UseVisualStyleBackColor = true;
            this.StopRec.Click += new System.EventHandler(this.StopRec_Click);
            // 
            // StartRec
            // 
            this.StartRec.Location = new System.Drawing.Point(12, 181);
            this.StartRec.Name = "StartRec";
            this.StartRec.Size = new System.Drawing.Size(136, 43);
            this.StartRec.TabIndex = 4;
            this.StartRec.Text = "Start recording";
            this.StartRec.UseVisualStyleBackColor = true;
            this.StartRec.Click += new System.EventHandler(this.StartRec_Click);
            // 
            // debugX
            // 
            this.debugX.Location = new System.Drawing.Point(241, 309);
            this.debugX.Name = "debugX";
            this.debugX.Size = new System.Drawing.Size(100, 20);
            this.debugX.TabIndex = 5;
            this.debugX.Text = "0";
            // 
            // debugY
            // 
            this.debugY.Location = new System.Drawing.Point(241, 335);
            this.debugY.Name = "debugY";
            this.debugY.Size = new System.Drawing.Size(100, 20);
            this.debugY.TabIndex = 6;
            this.debugY.Text = "0";
            // 
            // debugZ
            // 
            this.debugZ.Location = new System.Drawing.Point(241, 361);
            this.debugZ.Name = "debugZ";
            this.debugZ.Size = new System.Drawing.Size(100, 20);
            this.debugZ.TabIndex = 7;
            this.debugZ.Text = "0";
            // 
            // statusCode
            // 
            this.statusCode.AutoSize = true;
            this.statusCode.Location = new System.Drawing.Point(27, 13);
            this.statusCode.Name = "statusCode";
            this.statusCode.Size = new System.Drawing.Size(100, 13);
            this.statusCode.TabIndex = 9;
            this.statusCode.Text = "NOT CONNECTED";
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(358, 297);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(125, 43);
            this.connectButton.TabIndex = 10;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.ConnectButton_Click);
            // 
            // updateData
            // 
            this.updateData.Location = new System.Drawing.Point(358, 346);
            this.updateData.Name = "updateData";
            this.updateData.Size = new System.Drawing.Size(125, 43);
            this.updateData.TabIndex = 11;
            this.updateData.Text = "UpdateData";
            this.updateData.UseVisualStyleBackColor = true;
            this.updateData.Click += new System.EventHandler(this.UpdateData_Click);
            // 
            // finishVrep
            // 
            this.finishVrep.Location = new System.Drawing.Point(358, 395);
            this.finishVrep.Name = "finishVrep";
            this.finishVrep.Size = new System.Drawing.Size(125, 43);
            this.finishVrep.TabIndex = 12;
            this.finishVrep.Text = "Finish V-REP";
            this.finishVrep.UseVisualStyleBackColor = true;
            this.finishVrep.Click += new System.EventHandler(this.FinishVrep_Click);
            // 
            // imageBox1
            // 
            this.imageBox1.Location = new System.Drawing.Point(377, 84);
            this.imageBox1.Name = "imageBox1";
            this.imageBox1.Size = new System.Drawing.Size(75, 23);
            this.imageBox1.TabIndex = 2;
            this.imageBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(659, 205);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 450);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.imageBox1);
            this.Controls.Add(this.finishVrep);
            this.Controls.Add(this.updateData);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.statusCode);
            this.Controls.Add(this.debugZ);
            this.Controls.Add(this.debugY);
            this.Controls.Add(this.debugX);
            this.Controls.Add(this.StartRec);
            this.Controls.Add(this.StopRec);
            this.Controls.Add(this.ButtonStart);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imageBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonStart;
        private System.Windows.Forms.Button StopRec;
        private System.Windows.Forms.Button StartRec;
        private System.Windows.Forms.TextBox debugX;
        private System.Windows.Forms.TextBox debugY;
        private System.Windows.Forms.TextBox debugZ;
        private System.Windows.Forms.Label statusCode;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Button updateData;
        private System.Windows.Forms.Button finishVrep;
        private Emgu.CV.UI.ImageBox imageBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

