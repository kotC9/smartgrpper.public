﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using SmartGripper.Common.GlobalVariables;
using SmartGripper.Robot;
using SmartGripper.UserForm.Database;
using SmartGripper.UserForm.Detecting;
using SmartGripper.UserForm.Models;
using SmartGripper.UserForm.SpeechRecognition;
using SmartGripper.UserForm.VREP;
using SmartGripper.UserForm.Recording;
/*
 * kotC9 aka Sokolov S. developer
 * contacts:
 * kotc9.icu (habr, github, vk)
 * svssys@ya.ru
 * Russia, Ekaterinburg
 * ----------------------------------
 * Kvantoriada, 'Advanced robot gripper'
 */


namespace SmartGripper.UserForm.Forms
{
    public partial class Form1 : Form
    {
        private Recorder _recorder;
        private CmdsVrep _cmdsVrep;
        private Detect _detect;
        public Form1()
        {
            //_detect = new Detect();

            InitializeComponent();

            _recorder = new Recorder();
            _cmdsVrep = new CmdsVrep();
            //_detect = new Detect();
            GlobalVariables.ListItems = new List<string>();
            GlobalVariables.ListCommands = new List<string>();
            CmdsItemDatabase.GetItemList(ref GlobalVariables.ListItems);
            SpeechCmdsDatabase.GetCommandList(ref GlobalVariables.ListCommands);
            
            //CmdsItemDatabase.AddTestItems();
            //SpeechCmdsDatabase.AddTestCommands();
        }

        private async void StartButton(object sender, EventArgs e)
        {
            if (!GlobalVariables.IsSpeechRecognized)
            {
                MessageBox.Show("please press the button 'Start recording' before starting");
                return;
            }

            if (GlobalVariables.UseOnlyVrep)
            {
                var imageVrep = _cmdsVrep.GetImageFromCamera();

                //detect items
                var items = _detect.DetectObjects(imageVrep);
                foreach (var item in items)
                {
                    //Maybe remove
                    if (item.Confidence < 0.5f) continue;

                    foreach (var temp in GlobalVariables.ItemsToDetecting)
                    {
                        if (item.Type != temp) continue;

                        //founded item
                        //Z - const
                        _cmdsVrep.MoveToPos(new Point3D(item.Center().X,item.Center().Y,0));
                        //wait
                        await Task.Run(() =>
                        {
                            while (!_cmdsVrep.OkTask())
                            {
                                //wait
                            }
                        });
                        _cmdsVrep.GrabItem();
                    }
                }
            }
            
            /*
            
            var testBitmap = detect.MakeBitmap();
            var testByte = detect.ConvertBitmapToBytes(testBitmap);
            var items = detect.DetectObjects(testByte);
            foreach (var item in items)
                label1.Text += $"{item.Type}: {item.Confidence}\n";*/
        }

        

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// start recording audio
        /// </summary>
        private void StartRec_Click(object sender, EventArgs e)
        {
            _recorder.OnStartRecording();
        }

        /// <summary>
        /// stop recording audio and recognition it
        /// </summary>
        private void StopRec_Click(object sender, EventArgs e)
        {
            _recorder.OnStopRecording();
            var temp = _recorder.OggToByteArray(@"D:\Users\kotc9-lapka\Documents\speech.ogg");

            var text = Recognition.YandexApiRecognition(temp);
            if (Recognition.CheckTextSpeech(text))
            {
                GlobalVariables.IsSpeechRecognized = true;
                label1.Text = text;
            }
        }

        private void ConnectButton_Click(object sender, EventArgs e)
        {
            _cmdsVrep?.ConnectToVrep();
        }

        private void UpdateData_Click(object sender, EventArgs e)
        {
            _cmdsVrep?.MoveToPos(new Point3D(Convert.ToInt32(debugX.Text), Convert.ToInt32(debugY.Text), Convert.ToInt32(debugZ.Text)));

            if (_cmdsVrep != null)
            {
                var imageVrep = _cmdsVrep.GetImageFromCamera();
                
            }
        }

        private void FinishVrep_Click(object sender, EventArgs e)
        {
            _cmdsVrep?.FinishVrep();
        }
    }
}
