﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGripper.UserForm.Models
{
    public class ItemDatabase
    {
        [Key]
        public int Id { get; set; }
        public string ItemClass { get; set; }
        public int GripperType { get; set; }
    }
}
