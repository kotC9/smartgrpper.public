﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SmartGripper.UserForm.Models
{
    public class CommandDatabase
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }

        public int Task { get; set; }
    }
}
