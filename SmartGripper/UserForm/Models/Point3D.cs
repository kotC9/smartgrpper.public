﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGripper.UserForm.Models
{
    /// <summary>
    /// TODO: не изобретать велосипед, найти готовый класс Point3D
    /// </summary>
    public class Point3D
    {
        public float X { get; set; }

        public float Y { get; set; }

        public float Z { get; set; }

        public Point3D(int ix, int iy, int iz)
        {
            X = ix;
            Y = iy;
            Z = iz;
        }
    }
}
