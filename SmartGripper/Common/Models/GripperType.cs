﻿namespace SmartGripper.Common.Models
{
    /// <summary>
    /// gripper type for VREP and KUKA Robot
    /// </summary>
    public enum GripperType
    {
        ElectromechanicalGripper = 0,
        SuckerGripper = 1 
    }
}
