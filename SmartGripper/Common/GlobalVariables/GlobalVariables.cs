﻿using System.Collections.Generic;
/* kotC9 */

namespace SmartGripper.Common.GlobalVariables
{
    /// <summary>
    /// Global variables
    /// </summary>
    public class GlobalVariables
    {
        public static List<string> ListItems;
        public static List<string> ListCommands;

        //used only 1 item
        public static List<string> ItemsToDetecting;

        public static bool IsSpeechRecognized = false;
        public static bool UseOnlyVrep = false;
    }
}
