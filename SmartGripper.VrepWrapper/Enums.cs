﻿namespace SmartGripper.VrepWrapper
{
    // Command return codes
    public enum CommandReturnCodes
    {
        SimxReturnOk = 0,
        SimxReturnNovalueFlag = 1,
        SimxReturnTimeoutFlag = 2,
        SimxReturnIllegalOpmodeFlag = 4,
        SimxReturnRemoteErrorFlag = 8,
        SimxReturnSplitProgressFlag = 16,
        SimxReturnLocalErrorFlag = 32,
        SimxReturnInitializeErrorFlag = 64,
    }

    // Regular operation modes
    public enum RegularOperationMode
    {
        // Regular operation modes
        SimxOpmodeOneshot = 0,
        SimxOpmodeBlocking = 65536,
        SimxOpmodeOneshotWait = 65536,
        SimxOpmodeContinuous = 131072,
        SimxOpmodeStreaming = 131072,

        // Operation modes for heavy data
        SimxOpmodeOneshotSplit = 196608,
        SimxOpmodeContinuousSplit = 262144,
        SimxOpmodeStreamingSplit = 262144,

        // Special operation modes
        SimxOpmodeDiscontinue = 327680,
        SimxOpmodeBuffer = 393216,
        SimxOpmodeRemove = 458752,
    }

    // Scene object types
    public enum SceneObjectTypes
    {
        SimObjectShapeType = 0,
        SimObjectJointType = 1,
        SimObjectGraphType = 2,
        SimObjectCameraType = 3,
        SimObjectDummyType = 4,
        SimObjectProximitysensorType = 5,
        SimObjectReserved1 = 6,
        SimObjectReserved2 = 7,
        SimObjectPathType = 8,
        SimObjectVisionsensorType = 9,
        SimObjectVolumeType = 10,
        SimObjectMillType = 11,
        SimObjectForcesensorType = 12,
        SimObjectLightType = 13,
        SimObjectMirrorType = 14,
    }

    // General object types
    public enum GeneralObjectTypes
    {
        SimAppobjObjectType = 109,
        SimAppobjCollisionType = 110,
        SimAppobjDistanceType = 111,
        SimAppobjSimulationType = 112,
        SimAppobjIkType = 113,
        SimAppobjConstraintsolverType = 114,
        SimAppobjCollectionType = 115,
        SimAppobjUiType = 116,
        SimAppobjScriptType = 117,
        SimAppobjPathplanningType = 118,
        SimAppobjReservedType = 119,
        SimAppobjTextureType = 120,
    }

    // Inverse Kinematics calculation methods
    public enum IkCalculationMethods
    {
        SimIkPseudoInverseMethod = 0,
        SimIkDampedLeastSquaresMethod = 1,
        SimIkJacobianTransposeMethod = 2,
    }

    // Inverse Kinematics constraints
    public enum IkConstraints
    {
        SimIkXConstraint = 1,
        SimIkYConstraint = 2,
        SimIkZConstraint = 4,
        SimIkAlphaBetaConstraint = 8,
        SimIkGammaConstraint = 16,
        SimIkAvoidanceConstraint = 64,
    }

    //  Inverse Kinematics calculation results
    public enum IkCalculationResults
    {
        SimIkresultNotPerformed = 0,
        SimIkresultSuccess = 1,
        SimIkresultFail = 2,
    }

    // Scene object sub-types
    public enum SceneObjectSubTypes
    {
        SimLightOmnidirectionalSubtype = 1,
        SimLightSpotSubtype = 2,
        SimLightDirectionalSubtype = 3,
        SimJointRevoluteSubtype = 10,
        SimJointPrismaticSubtype = 11,
        SimJointSphericalSubtype = 12,
        SimShapeSimpleshapeSubtype = 20,
        SimShapeMultishapeSubtype = 21,
        SimProximitysensorPyramidSubtype = 30,
        SimProximitysensorCylinderSubtype = 31,
        SimProximitysensorDiscSubtype = 32,
        SimProximitysensorConeSubtype = 33,
        SimProximitysensorRaySubtype = 34,
        SimMillPyramidSubtype = 40,
        SimMillCylinderSubtype = 41,
        SimMillDiscSubtype = 42,
        SimMillConeSubtype = 42,
        SimObjectNoSubtype = 200,
    }

    // Scene object main properties
    public enum SceneObjectMainProperties
    {
        SimObjectspecialpropertyCollidable = 1,
        SimObjectspecialpropertyMeasurable = 2,
        SimObjectspecialpropertyDetectableUltrasonic = 16,
        SimObjectspecialpropertyDetectableInfrared = 32,
        SimObjectspecialpropertyDetectableLaser = 64,
        SimObjectspecialpropertyDetectableInductive = 128,
        SimObjectspecialpropertyDetectableCapacitive = 256,
        SimObjectspecialpropertyRenderable = 512,
        SimObjectspecialpropertyDetectableAll = 496,
        SimObjectspecialpropertyCuttable = 1024,
        SimObjectspecialpropertyPathplanningIgnored = 2048,
    }

    // Model properties
    public enum ModelProperties
    {
        SimModelpropertyNotCollidable = 1,
        SimModelpropertyNotMeasurable = 2,
        SimModelpropertyNotRenderable = 4,
        SimModelpropertyNotDetectable = 8,
        SimModelpropertyNotCuttable = 16,
        SimModelpropertyNotDynamic = 32,
        SimModelpropertyNotRespondable = 64,
        SimModelpropertyNotReset = 128,
        SimModelpropertyNotVisible = 256,
        SimModelpropertyNotModel = 61440,
    }

    //Sim Messages
    public enum SimMessages
    {
        SimMessageUiButtonStateChange = 0,
        SimMessageReserved9 = 1,
        SimMessageObjectSelectionChanged = 2,
        SimMessageReserved10 = 3,
        SimMessageModelLoaded = 4,
        SimMessageReserved11 = 5,
        SimMessageKeypress = 6,
        SimMessageBannerclicked = 7,
        SimMessageForCApiOnlyStart = 256,
        SimMessageReserved1 = 257,
        SimMessageReserved2 = 258,
        SimMessageReserved3 = 259,
        SimMessageEventcallbackScenesave = 260,
        SimMessageEventcallbackModelsave = 261,
        SimMessageEventcallbackModuleopen = 262,
        SimMessageEventcallbackModulehandle = 263,
        SimMessageEventcallbackModuleclose = 264,
        SimMessageReserved4 = 265,
        SimMessageReserved5 = 266,
        SimMessageReserved6 = 267,
        SimMessageReserved7 = 268,
        SimMessageEventcallbackInstancepass = 269,
        SimMessageEventcallbackBroadcast = 270,
        SimMessageEventcallbackImagefilterEnumreset = 271,
        SimMessageEventcallbackImagefilterEnumerate = 272,
        SimMessageEventcallbackImagefilterAdjustparams = 273,
        SimMessageEventcallbackImagefilterReserved = 274,
        SimMessageEventcallbackImagefilterProcess = 275,
        SimMessageEventcallbackReserved1 = 276,
        SimMessageEventcallbackReserved2 = 277,
        SimMessageEventcallbackReserved3 = 278,
        SimMessageEventcallbackReserved4 = 279,
        SimMessageEventcallbackAbouttoundo = 280,
        SimMessageEventcallbackUndoperformed = 281,
        SimMessageEventcallbackAbouttoredo = 282,
        SimMessageEventcallbackRedoperformed = 283,
        SimMessageEventcallbackScripticondblclick = 284,
        SimMessageEventcallbackSimulationabouttostart = 285,
        SimMessageEventcallbackSimulationended = 286,
        SimMessageEventcallbackReserved5 = 287,
        SimMessageEventcallbackKeypress = 288,
        SimMessageEventcallbackModulehandleinsensingpart = 289,
        SimMessageEventcallbackRenderingpass = 290,
        SimMessageEventcallbackBannerclicked = 291,
        SimMessageEventcallbackMenuitemselected = 292,
        SimMessageEventcallbackRefreshdialogs = 293,
        SimMessageEventcallbackSceneloaded = 294,
        SimMessageEventcallbackModelloaded = 295,
        SimMessageEventcallbackInstanceswitch = 296,
        SimMessageEventcallbackGuipass = 297,
        SimMessageEventcallbackMainscriptabouttobecalled = 298,
        SimMessageEventcallbackRmlposition = 299,
        SimMessageEventcallbackRmlvelocity = 300,

        SimMessageSimulationStartResumeRequest = 4096,
        SimMessageSimulationPauseRequest = 4097,
        SimMessageSimulationStopRequest = 4098,
    }

    // Scene object properties
    public enum SceneObjectProperties
    {
        SimObjectpropertyCollapsed = 16,
        SimObjectpropertySelectable = 32,
        SimObjectpropertyReserved7 = 64,
        SimObjectpropertySelectmodelbaseinstead = 128,
        SimObjectpropertyDontshowasinsidemodel = 256,
        SimObjectpropertyCanupdatedna = 1024,
        SimObjectpropertySelectinvisible = 2048,
        SimObjectpropertyDepthinvisible = 4096,
    }

    // Remote API message header structure
    public enum MessageHeaderStructure
    {
        SimxHeaderoffsetCrc = 0,
        SimxHeaderoffsetVersion = 2,
        SimxHeaderoffsetMessageId = 3,
        SimxHeaderoffsetClientTime = 7,
        SimxHeaderoffsetServerTime = 11,
        SimxHeaderoffsetSceneId = 15,
        SimxHeaderoffsetServerState = 17,
    }

    // Remote API command header
    public enum CommandHeader
    {
        SimxCmdheaderoffsetMemSize = 0,
        SimxCmdheaderoffsetFullMemSize = 4,
        SimxCmdheaderoffsetPdataOffset0 = 8,
        SimxCmdheaderoffsetPdataOffset1 = 10,
        SimxCmdheaderoffsetCmd = 14,
        SimxCmdheaderoffsetDelayOrSplit = 18,
        SimxCmdheaderoffsetSimTime = 20,
        SimxCmdheaderoffsetStatus = 24,
        SimxCmdheaderoffsetReserved = 25,
    }
}
